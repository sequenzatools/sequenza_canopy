
import_sequenza <- function(dir.path, cellularity = NULL, ploidy = NULL) {
   segfile <- dir(dir.path, "_segments.txt")
   if (length(segfile) != 1) {
      stop("This does not look like a sequenza results folder: ", dir.path)
   } else {
      sample_id  <- unlist(strsplit(segfile,
                                    "_segments.txt")
                          )
      files_list <- sequenza_files(dir.path = dir.path,
                                  sample.id =  sample_id)
      segments   <- read.table(files_list["segments"], header = TRUE,
                               sep = "\t", stringsAsFactors = FALSE)
      mutations  <- read.table(files_list["mutations"], header = TRUE,
                               sep = "\t", stringsAsFactors = FALSE)
      solutions  <- read.table(files_list["solutions"], header = TRUE,
                               sep = "\t", stringsAsFactors = FALSE)
      extract  <- eval(as.name(load(files_list["extract"])))
      if (is.null(cellularity) | is.null(ploidy)) {
          CP  <- eval(as.name(load(files_list["cp"])))
          cint <- get.ci(CP)
          cellularity <- cint$max.cellularity
          ploidy <- cint$max.ploidy
      }
      list(solutions = solutions, segments = segments,
         mutations = mutations, extract = extract,
         cellularity = cellularity, ploidy = ploidy)
   }
}

import_patient <- function(dir.paths, results.type = "sequenza") {
   sectors <- dir.paths
   sectors <- sectors[file.info(sectors)$isdir]
   s_names <- basename(sectors)
   if (results.type == "sequenza") {
      sectors <- sapply(sectors, import_sequenza, simplify = FALSE)
      names(sectors) <- s_names
      sectors
   }
}

import_genotype <- function(sectors, raw.files, import.type = "sequenza") {
   if (sum(names(sectors) %in% names(raw.files)) == length(sectors)) {
      if (import.type == "sequenza") {
         # common mutaions
         mutations <- mutations_depths(sectors = sectors)
         # Retrieve the genotype of each segments

      }
   } else {
      stop("Not all sectors are associated with a raw file")
   }

}

sequenza_files <- function(dir.path, sample.id) {
   make_filename <- function(x) file.path(dir.path,
       paste(sample.id, x, sep = "_"))
   #cp.file   <- make_filename("CP_contours.pdf")
   #cint.file <- make_filename("confints_CP.txt")
   #chrw.file <- make_filename("chromosome_view.pdf")
   #geno.file <- make_filename("genome_view.pdf")
   #cn.file   <- make_filename("CN_bars.pdf")
   #fit.file  <- make_filename("model_fit.pdf")
   alt.file  <- make_filename("alternative_solutions.txt")
   #afit.file <- make_filename("alternative_fit.pdf")
   muts.file <- make_filename("mutations.txt")
   segs.file <- make_filename("segments.txt")
   robj.extr <- make_filename("sequenza_extract.RData")
   robj.fit  <- make_filename("sequenza_cp_table.RData")

   f_list    <- list(solutions = alt.file,
                     segments  = segs.file,
                     mutations = muts.file,
                     extract   = robj.extr,
                     cp        = robj.fit)
   f_check   <- unlist(lapply(f_list, file.exists))
   f_check   <- which( (f_check == FALSE))
   if (length(f_check) != 0) {
      missing <- paste(f_list[f_check], collapse = ", ")
      stop(paste("Missing files", missing, sep = " : "))
   } else {
      unlist(f_list)
   }
}
