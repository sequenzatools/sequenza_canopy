\name{import_patient}
\title{Load all the patients results for different sector}

\description{
   Load all the patients results for different sector
 }

\usage{
  import_patient(dir.paths, results.type = "sequenza")
}

\arguments{
    \item{dir.paths}{List of directories, each containing a sample result}
    \item{results.type}{Type of the results to load}
}
